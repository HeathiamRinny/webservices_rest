DROP TABLE IF EXISTS `Clients` ;
DROP TABLE IF EXISTS `Orders` ;
DROP TABLE IF EXISTS `Articles` ;
DROP TABLE IF EXISTS `Orders_has_Articles` ;

CREATE TABLE IF NOT EXISTS `Clients` (`id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(45),
    `password` VARCHAR(45),
    `mail` VARCHAR(45),
    `orderNumber` INT,
    PRIMARY KEY (`id`));



CREATE TABLE IF NOT EXISTS `Orders` (`id` INT NOT NULL AUTO_INCREMENT,
    `nbArticle` INT,
    `cost` DECIMAL,
    `Clients_id` INT NOT NULL,
    PRIMARY KEY (`id`, `Clients_id`),
    CONSTRAINT `fk_Orders_Clients`
    FOREIGN KEY (`Clients_id`)
    REFERENCES `Clients` (`id`));




CREATE TABLE IF NOT EXISTS `Articles` (`id` INT NOT NULL AUTO_INCREMENT,
    `designation` VARCHAR(45) NULL,
    `quantity` INT NULL,
    `price` DECIMAL NULL,
    PRIMARY KEY (`id`));


CREATE TABLE IF NOT EXISTS `Orders_has_Articles` (
    `Orders_id` INT NOT NULL,
    `Orders_Clients_id` INT NOT NULL,
    `Articles_id` INT NOT NULL,
    PRIMARY KEY (`Orders_id`, `Orders_Clients_id`, `Articles_id`),
    FOREIGN KEY (`Orders_id` , `Orders_Clients_id`) REFERENCES `Orders` (`id` , `Clients_id`)
    FOREIGN KEY (`Articles_id`) REFERENCES `Articles` (`id`));